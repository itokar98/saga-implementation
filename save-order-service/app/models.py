from uuid import UUID

from pydantic import BaseModel


class ProductCart(BaseModel):
    id: int
    name: str
    count: int


class Order(BaseModel):
    UUID: UUID
    products: list[ProductCart]
    coutProducts: int
    price: int
    paymentUUID: UUID
    statusPayment: int
