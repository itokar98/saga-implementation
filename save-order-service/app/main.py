import json
import os

import motor.motor_asyncio
from fastapi import FastAPI, status
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

from app.models import Order

app = FastAPI()

MONGO_DETAILS = os.environ['MONGO_DETAILS']
client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

db = client.orders


@app.post("/", status_code=status.HTTP_201_CREATED)
def save_order(order: Order):
    order_json = jsonable_encoder(order)
    db['orders'].insert_one(order_json)
    return order
