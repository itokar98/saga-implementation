# spels-make-template-django


## Highlights

- Modern Python development with Python 3.9+
- Bleeding edge Django 3.1+
- Fully dockerized, local development via docker-compose.
- PostgreSQL 13.1+
- Start off with full test coverage and [continuous integration](https://github.com/agconti/cookiecutter-django-rest/blob/master/%7B%7Bcookiecutter.github_repository_name%7D%7D/.travis.yml).
- Complete [Django Rest Framework](http://www.django-rest-framework.org/) integration
- Always current dependencies and security updates enforced by [pyup.io](https://pyup.io/).
- A slim but robust foundation -- just enough to maximize your productivity, nothing more.



## Quick Start

Install [cookiecutter](https://github.com/audreyr/cookiecutter):

```bash
pip install cookiecutter jinja2-git
```

Scaffold your project:
```bash
cookiecutter gl:itokar98/spels-make-template
```

## License

MIT. See [LICENSE]() for more details.
