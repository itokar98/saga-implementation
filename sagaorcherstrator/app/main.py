import requests
from fastapi import FastAPI, HTTPException

from app.models import Cart

ROUND_ROBIN_TABLE = {
    'order': [
        {'host': 'http://localhost:8081', 'load': 10},
        {'host': 'http://localhost:8082', 'load': 30},
    ],
    'payment': [
        {'host': 'http://localhost:8082', 'load': 40},
        {'host': 'http://localhost:8084', 'load': 90},
    ],
    'save_order': [
        {'host': 'http://localhost:8083', 'load': 20},
        {'host': 'http://localhost:8086', 'load': 80},
    ]
}

app = FastAPI()


class SagaOrchestrator():
    """Implementation saga"""

    def start(self, data):
        saga_order_service = self.get_free_server('order')
        response_saga_order = self.request(saga_order_service, data.json())
        
        if(response_saga_order.status_code != 200):
            error_data = response_saga_order.json()
            raise HTTPException(
                status_code=response_saga_order.status_code,
                detail=error_data['detail'],
            )
        
        saga_pay_service = self.get_free_server('payment')
        response_saga_pay = self.request(saga_pay_service, response_saga_order)

        if(response_saga_pay.status_code != 200):
            error_data = response_saga_pay.json()
            raise HTTPException(
                status_code=response_saga_pay.status_code,
                detail=error_data['detail'],
            )

        saga_save_service = self.get_free_server('save_order')
        response_saga_save = self.request(saga_save_service, response_saga_pay)

        if(response_saga_save.status_code != 201):
            error_data = response_saga_save.json()
            raise HTTPException(
                status_code=response_saga_save.status_code,
                detail=error_data['detail'],
            )

        return response_saga_save.json()

    def get_free_server(self, key_service):
        min_load = min(
            ROUND_ROBIN_TABLE[key_service],
            key=lambda ROUND_ROBIN_TABLE: ROUND_ROBIN_TABLE['load'],
        )
        return min_load['host']

    def request(self, service, data):
        response = requests.post(url=service, data=data)
        return response


@app.post("/")
def read_item(cart: Cart):
    saga = SagaOrchestrator()
    response_saga = saga.start(cart)
    return response_saga
