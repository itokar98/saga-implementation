import os
import uuid

from fastapi import FastAPI, HTTPException

from app.models import Order, OrderWithPayment

app = FastAPI()


@app.post("/")
def read_item(order: Order):
    ERROR_VAR = int(os.environ["ERROR_VAR"])
    os.environ["ERROR_VAR"] = str(ERROR_VAR + 1)
    print(os.environ["ERROR_VAR"])
    if ERROR_VAR % 2 != 0:
        raise HTTPException(status_code=502, detail='Error create payment')
    orderUUID = uuid.uuid1()
    statusPay = 0
    order = OrderWithPayment(
        UUID=order.UUID,
        products=order.products,
        coutProducts=order.coutProducts,
        price=order.price,
        paymentUUID=orderUUID,
        statusPayment=statusPay,
    )
    return order
