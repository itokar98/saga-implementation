from typing import Optional
from uuid import uuid1

from fastapi import FastAPI, HTTPException

from app.models import Cart, Order, Product

app = FastAPI()

Products: list[Product] = [
    Product(id=1, name='tomato', price=100),
]


@app.post("/")
def read_item(cart: Cart):
    price = 0
    count = 0
    if len(cart.products) == 0:
        raise HTTPException(status_code=400, detail="Cart empty")
    for i in Products:
        for j in cart.products:
            if j.id == i.id:
                count += j.count
                price += j.count * i.price
    if count == 0:
        raise HTTPException(status_code=404, detail="Product not found")
    order: Order = Order(
        UUID=uuid1(),
        products=cart.products,
        coutProducts=count,
        price=price
    )
    return order

