from uuid import UUID

from pydantic import BaseModel


class Product(BaseModel):
    id: int
    name: str
    price: int


class ProductCart(BaseModel):
    id: int
    name: str
    count: int


class Cart(BaseModel):
    products: list[ProductCart]


class Order(BaseModel):
    UUID: UUID
    products: list[ProductCart]
    coutProducts: int
    price: int
